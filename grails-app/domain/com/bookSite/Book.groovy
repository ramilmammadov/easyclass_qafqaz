package com.bookSite

class Book {
    String title
    String fileName
    int status
    static belongsTo = [teacher:User]
    static constraints = {
    }
}
