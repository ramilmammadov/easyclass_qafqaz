class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(controller: 'home')
		"/admin"(controller: 'admin',action: 'bookList')
		"500"(view:'/error')
	}
}
