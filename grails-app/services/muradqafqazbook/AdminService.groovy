package muradqafqazbook

import com.bookSite.Book
import com.bookSite.User

class AdminService {

   def deleteBook(id){
       Book?.get(id)?.status=0
   }
    def deleteUser(id){
        User?.get(id)?.enabled=0
    }
}
