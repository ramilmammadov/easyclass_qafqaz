package muradqafqazbook

import com.bookSite.Role
import com.bookSite.User
import com.bookSite.UserRole

class HomeService {
    def saveUser(params) {
        def user = new User()
        user?.username = params?.username
        user?.password = params?.password
        user?.name = params?.name
        user?.surname = params?.surname
        user?.enabled=true
        user?.passwordExpired=false
        user?.accountExpired=false
        user?.accountLocked=false
        user?.save(flush: true, failOnError: true)

        def role
        if(params?.id?.equals('student'))
             role = Role?.findByAuthority("ROLE_STUDENT")
        else
            role = Role?.findByAuthority("ROLE_TEACHER")
        new UserRole(user: user, role: role)?.save(flush: true, failOnError: true)
    }
}
