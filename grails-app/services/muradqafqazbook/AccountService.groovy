package muradqafqazbook

import com.bookSite.Book

class AccountService {

    def saveBook(params, request,user) {
        def book=new Book()
        book?.title=params?.title
        book?.status=1
        def files = request.getFiles("pdf")
        if (files) {
            for (file in files) {
                if (file && !file.empty) {
                    def matcher = (file.getOriginalFilename() =~ /.*\.(.*)$/)
                    def extension
                    if (matcher.matches()) {
                        extension = matcher[0][1]
                    }
                    if (extension == 'pdf') {
                        String fileName = UUID.randomUUID().toString() + "." + extension
                        String dir = '/home/projects/muradbook/books'
                        def filePath = new File(dir)
                        filePath.mkdirs()
                        file.transferTo(new File(filePath, fileName))
                        book?.fileName = fileName
                    }
                }

            }
        }
        book?.teacher=user
        book?.save(flush: true,failOnError: true)
    }

    def removeBook(params){
        def book=Book?.get(params?.getLong('id'))
        book?.status=0
        book?.save(flush: true,failOnError: true)

    }


}
