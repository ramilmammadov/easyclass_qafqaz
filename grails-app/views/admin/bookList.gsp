<meta name="layout" content="mainAdmin"/>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Kitabın adı</th>
                <th>Müəllif</th>
                <th>Sil</th>
            </tr>
        </thead>
        <tbody>
        <g:each in="${bookList}" var="book">
            <tr>
                <td>${book?.title}</td>
                <td>${book?.teacher?.name+" "+book?.teacher?.surname}</td>
                <td><g:link action="deleteBook" id="${book?.id}"><span
                        class="glyphicon glyphicon-trash" aria-hidden="true"></span></g:link></td>
            </tr>
        </g:each>
        </tbody>
    </table>