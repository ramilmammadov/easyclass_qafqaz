<meta name="layout" content="mainAdmin"/>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>Adı</th>
        <th>Soyadı</th>
        <th>E-mail adresi</th>
        <th>Kitabların siyahısı</th>
        <th>Deaktiv et</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${teachers}" var="teacherRole">
        <g:if test="${teacherRole?.user?.enabled}">
            <tr>
                <td>${teacherRole?.user?.name}</td>
                <td>${teacherRole?.user?.surname}</td>
                <td>${teacherRole?.user?.username}</td>
                <td><g:link action="bookList" id="${teacherRole?.user?.id}"><span
                        class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></g:link></td>
                <td><g:link action="deleteUser" id="${teacherRole?.user?.id}"><span
                        class="glyphicon glyphicon-trash" aria-hidden="true"></span></g:link></td>
            </tr>
        </g:if>
    </g:each>
    </tbody>
</table>