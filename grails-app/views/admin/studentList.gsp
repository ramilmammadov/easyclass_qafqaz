<meta name="layout" content="mainAdmin"/>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Adı</th>
            <th>Soyadı</th>
            <th>E-mail adresi</th>
            <th>Deaktiv et</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${students}" var="studentRole">
            <g:if test="${studentRole?.user?.enabled}">
            <tr>
                <td>${studentRole?.user?.name}</td>
                <td>${studentRole?.user?.surname}</td>
                <td>${studentRole?.user?.username}</td>
                <td><g:link action="deleteUser" id="${studentRole?.user?.id}"><span
                        class="glyphicon glyphicon-trash" aria-hidden="true"></span></g:link></td>
            </tr>
            </g:if>
        </g:each>
        </tbody>
    </table>