<!DOCTYPE html>
<html lang="zxx">
<head>

    <!-- Meta -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1">

    <!-- Title -->
    <title>..:: Easy Class ::..</title>

    <!-- Favicon -->
    <link href="http://libraria.demo.presstigers.com/images/favicon.ico" rel="icon" type="image/x-icon"/>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i%7CLato:100,100i,300,300i,400,400i,700,700i,900,900i"
          rel="stylesheet"/>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="http://libraria.demo.presstigers.com/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

    <!-- Mobile Menu -->
    <link href="http://libraria.demo.presstigers.com/css/mmenu.css" rel="stylesheet" type="text/css"/>
    <link href="http://libraria.demo.presstigers.com/css/mmenu.positioning.css" rel="stylesheet" type="text/css"/>

    <!-- Stylesheet -->
    <link href="http://libraria.demo.presstigers.com/style.css" rel="stylesheet" type="text/css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="http://libraria.demo.presstigers.com/js/html5shiv.min.js"></script>
        <script src="http://libraria.demo.presstigers.com/js/respond.min.js"></script>
        <![endif]-->
</head>

<body>
<style>
    input[type=text], input[type=password]{
        border: 1px solid #538593!important;
    }
    .header-topbar a i,#home-v1-header-carousel .carousel-caption h2, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus{
        color: #6c9ea9;
    }
    button, input[type="button"], input[type="reset"], input[type="submit"],.btn-primary,.btn-success{
        background-color: #538593;
    }
    .underline {
        background-image: none;
    }
.dropdown-menu li a:hover{
            /*background-color:  #538593;*/
                }
</style>
<div id="wrap"><!-- Start: Header Section -->
    <header id="header-v1" class="navbar-wrapper">
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-default">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="navbar-header">
                                <div class="navbar-brand">
                                    <h1>
                                        <a href="${createLink(controller: 'home', action: 'index')}">
                                            <img src="${resource(dir: 'images', file: 'logo.png')}"
                                                 alt="LIBRARIA"/>
                                        </a>
                                    </h1>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-9">
                            <div class="header-topbar hidden-sm hidden-xs">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="topbar-info">
                                            <a href="tel:+994516133397"><i class="fa fa-phone"></i>+994 51 613 33 97</a>
                                            <span>/</span>
                                            <a href="mailto:info@bookportal.az"><i
                                                    class="fa fa-envelope"></i>info@bookportal.az</a>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                    </div>
                                </div>
                            </div>

                            <div class="navbar-collapse hidden-sm hidden-xs">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown active">
                                        <a data-toggle="dropdown" class="dropdown-toggle disabled"
                                           href="${createLink(controller: 'home',action: 'index')}">Home</a>

                                    </li>
                                    <sec:ifLoggedIn>
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle disabled"
                                               href="${createLink(controller: 'logout')}">Logout</a>
                                        </li>
                                    </sec:ifLoggedIn>
                                    <sec:ifNotLoggedIn>
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle disabled"
                                               href="#">Register</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="${createLink(controller: 'home',action: 'register',params: [type:'student'])}">Student</a></li>
                                                <li><a href="${createLink(controller: 'home',action: 'register',params: [type:'teacher'])}">Teacher</a></li>
                                            </ul>
                                        </li>
                                    </sec:ifNotLoggedIn>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="mobile-menu hidden-lg hidden-md">
                        <a href="#mobile-menu"><i class="fa fa-navicon"></i></a>

                        <div id="mobile-menu">
                            <ul>
                                <li class="mobile-title">
                                    <h4>Navigation</h4>
                                    <a href="#" class="close"></a>
                                </li>
                                <li>
                                    <a href="index.html">Home</a>
                                    <ul>
                                        <li><a href="index.html">Home V1</a></li>
                                        <li><a href="home-v2.html">Home V2</a></li>
                                        <li><a href="home-v3.html">Home V3</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="books-media-list-view.html">Books &amp; Media</a>
                                    <ul>
                                        <li><a href="books-media-list-view.html">Books &amp; Media List View</a></li>
                                        <li><a href="books-media-gird-view-v1.html">Books &amp; Media Grid View V1</a>
                                        </li>
                                        <li><a href="books-media-gird-view-v2.html">Books &amp; Media Grid View V2</a>
                                        </li>
                                        <li><a href="books-media-detail-v1.html">Books &amp; Media Detail V1</a></li>
                                        <li><a href="books-media-detail-v2.html">Books &amp; Media Detail V2</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="news-events-list-view.html">News &amp; Events</a>
                                    <ul>
                                        <li><a href="news-events-list-view.html">News &amp; Events List View</a></li>
                                        <li><a href="news-events-detail.html">News &amp; Events Detail</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">Pages</a>
                                    <ul>
                                        <li><a href="cart.html">Cart</a></li>
                                        <li><a href="checkout.html">Checkout</a></li>
                                        <li><a href="signin.html">Signin/Register</a></li>
                                        <li><a href="404.html">404/Error</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="blog.html">Blog</a>
                                    <ul>
                                        <li><a href="blog.html">Blog Grid View</a></li>
                                        <li><a href="blog-detail.html">Blog Detail</a></li>
                                    </ul>
                                </li>
                                <li><a href="services.html">Services</a></li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- End: Header Section -->

    <!-- Start: Slider Section -->

    <g:layoutBody/>
    <footer class="site-footer">
        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="footer-text col-md-3">
                        <p>&copy; 2017 LIBRARIA. All rights reserved.</p>
                    </div>

                    <div class="col-md-9 pull-right">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="books-media-list-view.html">Books &amp; Media</a></li>
                            <li><a href="news-events-list-view.html">News &amp; Events</a></li>
                            <li><a href="#">Kids &amp; Teens</a></li>
                            <li><a href="services.html">Services</a></li>
                            <li><a href="#">Research</a></li>
                            <li><a href="blog.html">Blog</a></li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End: Footer -->

    <!-- jQuery Latest Version 1.x -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/jquery-1.12.4.min.js"></script>

    <!-- jQuery UI -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/jquery-ui.min.js"></script>

    <!-- jQuery Easing -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/jquery.easing.1.3.js"></script>

    <!-- Bootstrap -->
    %{--<script type="text/javascript" src="http://libraria.demo.presstigers.com/js/bootstrap.min.js"></script>--}%

    <!-- Mobile Menu -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/mmenu.min.js"></script>

    <!-- Harvey - State manager for media queries -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/harvey.min.js"></script>

    <!-- Waypoints - Load Elements on View -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/waypoints.min.js"></script>

    <!-- Facts Counter -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/facts.counter.min.js"></script>

    <!-- MixItUp - Category Filter -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/mixitup.min.js"></script>

    <!-- Owl Carousel -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/owl.carousel.min.js"></script>

    <!-- Accordion -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/accordion.min.js"></script>

    <!-- Responsive Tabs -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/responsive.tabs.min.js"></script>

    <!-- Responsive Table -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/responsive.table.min.js"></script>

    <!-- Masonry -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/masonry.min.js"></script>

    <!-- Carousel Swipe -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/carousel.swipe.min.js"></script>

    <!-- bxSlider -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/bxslider.min.js"></script>

    <!-- Custom Scripts -->
    <script type="text/javascript" src="http://libraria.demo.presstigers.com/js/main.js"></script>
</div>

</body>
</html>