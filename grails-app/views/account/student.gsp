<meta name="layout" content="main"/>
<g:render template="underHeader"/>
<style>
    a{
        color:#538593;
    }
</style>
<div id="content" class="site-content">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="cart-main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page type-page status-publish hentry">
                                <div class="entry-content">
                                    <div class="woocommerce table-tabs r-tabs" id="responsiveTabs">
                                        <table class="table table-bordered shop_table cart">
                                            <thead>
                                            <tr>
                                                <th class="product-name">Title</th>
                                                <th class="product-quantity">Download</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <g:each in="${books}" var="book">
                                            <tr class="cart_item">
                                                    <td>${book?.title}</td>
                                                    <td><a href="${createLink(controller: "imageDisplayer", action: "displayPdf", params: [photoName: book?.fileName, dir: ''])}">Download</a> </td>
                                            </tr>
                                            </g:each>
                                            </tbody>
                                        </table>

                                    </div>
                                </div><!-- .entry-content -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>