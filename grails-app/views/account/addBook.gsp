<meta name="layout" content="main"/>
<g:render template="underHeader"/>
<div id="content" class="site-content">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="cart-main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page type-page status-publish hentry">
                                <div class="entry-content">
                                    <div class="woocommerce table-tabs r-tabs" id="responsiveTabs">
                                        <div class="col-md-5 col-md-offset-3">
                                            <g:uploadForm action="saveBook">
                                                <input type="text" name="title" placeholder="Title"/>
                                                <br/>
                                                <input accept="application/pdf," type="file" name="pdf" placeholder="file"/>
                                                <br/>
                                                <button type="submit" class="btn btn-success pull-right">Yüklə</button>
                                                <br/><br/><br/><br/>
                                            </g:uploadForm>
                                        </div>
                                    </div>
                                </div><!-- .entry-content -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>