<meta name="layout" content="main"/>
<g:render template="underHeader"/>
<div id="content" class="site-content">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="cart-main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page type-page status-publish hentry">
                                <div class="entry-content">
                                    <div class="woocommerce table-tabs r-tabs" id="responsiveTabs">
                                        <div class="col-md-6 col-md-offset-3">
                                            <g:form action="saveUser" id="${params?.type}">
                                                <input class="form-control" type="text" name="name" placeholder="Name"/>
                                                <input class="form-control" type="text" name="surname"
                                                       placeholder="Surname"/>
                                                <input class="form-control" type="text" name="username"
                                                       placeholder="E-mail"/>
                                                <input class="form-control" type="password" name="password"
                                                       placeholder="password"/>
                                                <button class="btn btn-success pull-right" type="submit">Save</button>
                                                <br/><br/><br/><br/><br/>
                                            </g:form>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>