<section class="page-banner services-banner">
    <div class="container">
        <div class="banner-header">
            <h2>${title}</h2>
            <span class="underline center"></span>
        </div>
    </div>
</section>