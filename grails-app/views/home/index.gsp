<meta name="layout" content="main"/>

<div data-ride="carousel" class="carousel slide" id="home-v1-header-carousel">

    <!-- Carousel slides -->
    <div class="carousel-inner">
        <div class="item active">
            <figure>
                <img alt="Home Slide"
                     src="http://libraria.demo.presstigers.com/images/header-slider/home-v1/header-slide.jpg"/>
            </figure>

            <div class="container">
                <div class="carousel-caption">
                    <h3>Oxumaq istədiyiniz kitabı elektron əldə edin.</h3>

                    <h2>ELEKTRON KİTABLAR PORTALI</h2>

                    <p>KİTABLAR SİSTEMƏ MÜƏLLİMLƏR TƏRƏFİNDƏN YÜKLƏNİLİR.</p>

                    <div class="slide-buttons hidden-sm hidden-xs">
                        <sec:ifNotLoggedIn>
                            <a href="${createLink(controller: 'account', action: 'student', params: [id: 'student'])}"
                               class="btn btn-primary">Student</a>

                            <a href="${createLink(controller: 'account', action: 'teacher', params: [id: 'teacher'])}"
                               class="btn btn-default">Teacher</a>
                        </sec:ifNotLoggedIn>

                        <sec:ifAnyGranted roles="ROLE_STUDENT">
                            <a href="${createLink(controller: 'account', action: 'student')}"
                               class="btn btn-primary">Student</a>
                        </sec:ifAnyGranted>
                        <sec:ifAnyGranted roles="ROLE_TEACHER">
                            <a href="${createLink(controller: 'account', action: 'teacher')}"
                               class="btn btn-default">Teacher</a>
                        </sec:ifAnyGranted>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Controls -->
    %{--<a class="left carousel-control" href="#home-v1-header-carousel" data-slide="prev"></a>--}%
    %{--<a class="right carousel-control" href="#home-v1-header-carousel" data-slide="next"></a>--}%
</div>
<g:if test="${message}">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>
    $(function () {
        swal("", "${message}", "error");
    });
 </script>
</g:if>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
