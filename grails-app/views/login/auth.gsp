<meta name="layout" content="main"/>
<g:render template="underHeader"/>
<br/><br/>
<style>
input {
    width: 300px;
    padding: 10px;
    border: 1px solid #538593;
}
</style>

<div style="width: 100%;text-align: center">
   <g:if test="${params?.login_error?.equals('1')}"><p style="color: red;margin: auto">Please fill the fields correctly</p></g:if>
  <br/>
    <form style="margin: auto" action='${postUrl}' method='POST' id='loginForm' class='login' autocomplete='off'>
        <p>
            <input placeholder="E-mail" type='text' class='text_' name='j_username' id='username'/>
        </p>

        <p>
            <input placeholder="password" type='password' class='text_' name='j_password' id='password'/>
        </p>

        <p>
            <input type='submit' id="submit" value='Login'/>
        </p>
    </form>

    %{--<a href="${createLink(controller: 'home', action: 'register')}"><button--}%
            %{--class="button btn btn-default">Register</button></a>--}%
    <br/><br/>

</div>