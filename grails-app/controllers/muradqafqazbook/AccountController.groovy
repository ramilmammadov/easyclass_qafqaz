package muradqafqazbook

import com.bookSite.Book
import grails.plugins.springsecurity.Secured

class AccountController {
    def accountService
    def springSecurityService

    @Secured(["ROLE_TEACHER"])
    def teacher() {
        def books=Book?.findAllByStatus(1)
        [title: 'BOOKS',books:books]
    }
    def deleteBook(){
        accountService.removeBook(params)
        redirect(action: 'teacher')
    }
    def addBook(){
        [title:'ADD BOOK']
    }
    def saveBook(){
        def user=springSecurityService.currentUser
        accountService.saveBook(params,request,user)
        redirect(action: 'teacher')
    }

    @Secured(["ROLE_STUDENT"])
    def student() {
        def books=Book?.findAllByStatus(1)
       [title: 'BOOKS',books:books]
    }
}
