package muradqafqazbook

import com.bookSite.Book
import com.bookSite.Role
import com.bookSite.User
import com.bookSite.UserRole
import grails.plugins.springsecurity.Secured

@Secured(["ROLE_ADMIN"])
class AdminController {
    def adminService

    def bookList() {
        def bookList
        if (params?.getLong('id')) {
            bookList = Book?.findAllByTeacherAndStatus(User?.get(params?.getLong('id')), 1)
        } else {
            bookList = Book?.findAllByStatus(1)
        }
        [bookList: bookList]
    }

    def studentList() {
        def students = UserRole?.findAllByRole(Role?.findByAuthority('ROLE_STUDENT'))
        [students: students]
    }

    def teacherList() {
        def teachers = UserRole?.findAllByRole(Role?.findByAuthority('ROLE_TEACHER'))
        [teachers: teachers]
    }

    def deleteBook() {
        adminService.deleteBook(params?.getLong('id'))
        redirect(action: 'bookList')
    }

    def deleteUser() {
        adminService.deleteUser(params?.getLong('id'))
        redirect(action: 'bookList')
    }
}
