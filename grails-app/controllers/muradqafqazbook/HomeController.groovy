package muradqafqazbook

class HomeController {
def homeService
    def cookieService
    def index() {
        if(this.cookieService.get("wrongaccount").equals("1")){
            this.cookieService.set("wrongaccount","0")
            [message:'Please add correct AccountController information.']
        }
    }
    def register() {
        if(!params?.type?.equals('student')&&!params?.type?.equals('teacher')){
            redirect(action: 'index')
        }
        [title:'REGISTER']
    }

    def saveUser(){
        homeService.saveUser(params)
        redirect(controller: 'login',action: 'auth')
    }
}
