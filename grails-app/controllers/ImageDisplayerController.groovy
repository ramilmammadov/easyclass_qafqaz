class ImageDisplayerController {
    def cookieService


    def displayPdf() {

        def photoPath = '/home/projects/muradbook/books'
        def photoName = params.photoName

        String specificFolder
        File photo

        try {
            if (photoName.isEmpty()) {
                specificFolder = "/home/projects/texnologiya/books/books"
                photo = new File(specificFolder + ".pdf")
                response.setHeader("Content-disposition", "inline")
            } else {
                specificFolder = photoPath
                photo = new File(specificFolder + File.separator + File.separator + photoName)
                response.setHeader("Content-disposition", "inline")
            }


            if (photo.exists()) {
                response.setContentType("application/pdf")
                response.outputStream << (photo.bytes)
                response.outputStream.flush()
                response.setHeader("Content-disposition", "inline")
            }
        } catch (Exception e) {
            e.stackTrace
        }
        return
    }

}

